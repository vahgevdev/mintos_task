package app.dagger

import android.content.Context
import app.AndroidApp
import app.dagger.modules.ApplicationModule
import dagger.Component
import data.dagger.modules.DataModule
import data.dagger.modules.NetworkModule
import data.dagger.scopes.AppScope
import domain.executor.PostExecutionThread
import domain.executor.ThreadExecutor
import domain.repository.CurrencyRepository
import domain.transformer.SchedulerTransformer

@AppScope
@Component(modules = [
    ApplicationModule::class,
    NetworkModule::class,
    DataModule::class
])
interface ApplicationComponent {
    //Exposed to sub-graphs.
    fun context(): Context

    fun threadExecutor(): ThreadExecutor

    fun postExecutionThread(): PostExecutionThread

    fun providesSchedulerTransformer(): SchedulerTransformer

    fun currencyRepository(): CurrencyRepository

    fun inject(app: AndroidApp)
}
