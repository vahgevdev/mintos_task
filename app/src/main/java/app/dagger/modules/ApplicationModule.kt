package app.dagger.modules

import android.app.Application
import android.content.Context
import app.ext.UIThread
import app.BuildConfig
import dagger.Module
import dagger.Provides
import data.dagger.qualifiers.ApiUrl
import data.dagger.scopes.AppScope
import data.executor.JobExecutor
import domain.executor.PostExecutionThread
import domain.executor.ThreadExecutor
import domain.transformer.AndroidSchedulerTransformer
import domain.transformer.SchedulerTransformer

/**
 * Dagger module that provides objects which will live during the application lifecycle.
 */
@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @AppScope
    internal fun provideApplicationContext(): Context {
        return this.application
    }

    @Provides
    @AppScope
    @ApiUrl
    internal fun apiUrl(): String {
        return BuildConfig.BASE_URL
    }

    @Provides
    @AppScope
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @AppScope
    internal fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @AppScope
    internal fun providesSchedulerTransformer(threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread): SchedulerTransformer {
        return AndroidSchedulerTransformer(threadExecutor, postExecutionThread)
    }
}
