package app.model.viewdata.currency

import domain.model.currency.CurrencyModel
import java.math.BigDecimal

data class CurrencyViewData(
    val title: String,
    val value: BigDecimal
) {
    companion object {

        private fun toViewData(
            title: String,
            value: BigDecimal
        ): CurrencyViewData {
            return CurrencyViewData(
                title = title,
                value = value
            )
        }

        fun toViewData(from: CurrencyModel): List<CurrencyViewData> {
            return from.rates.map { toViewData(it.key, it.value) }
        }
    }
}



