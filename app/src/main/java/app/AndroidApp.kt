package app

import android.app.Application
import android.content.Context
import app.dagger.ApplicationComponent
import app.dagger.DaggerApplicationComponent
import app.dagger.modules.ApplicationModule

class AndroidApp : Application() {

    private lateinit var applicationComponent: ApplicationComponent

    val component: ApplicationComponent
        get() = applicationComponent

    override fun onCreate() {
        super.onCreate()
        initializeApplicationComponent()

        component.inject(this)
    }

    private fun initializeApplicationComponent() {
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    companion object {

        operator fun get(context: Context): AndroidApp {
            return context.applicationContext as AndroidApp
        }
    }
}
