package app.ext

import android.content.Context
import android.widget.FrameLayout

open class BaseView(context: Context) : FrameLayout(context)
