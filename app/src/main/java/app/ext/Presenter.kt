package app.ext

import android.content.Intent
import android.util.Log

abstract class Presenter {

    open fun onResume() {
        Log.w(TAG, "Presenter onResume called, but no implementation found.")
    }

    open fun onPause() {
        Log.w(TAG, "Presenter onPause called, but no implementation found.")
    }

    open fun onStart() {
        Log.w(TAG, "Presenter onStart called, but no implementation found.")
    }

    open fun onStop() {
        Log.w(TAG, "Presenter onStop called, but no implementation found.")
    }

    open fun onCreate() {
        Log.w(TAG, "Presenter onCreate called, but no implementation found.")
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        Log.w(TAG, "Presenter onRequestPermissionsResult called, but no implementation found.")
    }

    open fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        Log.w(TAG, "Presenter onActivityResult called, but no implementation found.")
    }

    open fun onDestroy() {
        Log.w(TAG, "Presenter onDestroy called, but no implementation found.")
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity as appropriate.  If not handled, calls through to the super method.
     *
     * @return if the event has been handled.
     */
    fun onBackPressed(): Boolean {
        return false
    }

    companion object {
        private const val TAG = "Presenter"
    }
}
