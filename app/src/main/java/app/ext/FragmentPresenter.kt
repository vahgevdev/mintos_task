package app.ext

import android.content.Intent

abstract class FragmentPresenter : Presenter() {
    open fun onCreateView() {
        // NO-OP
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        // NO-OP
    }

    open fun onDetach() {
        // NO-OP
    }
}