package app.features.currency.mvp

import android.annotation.SuppressLint
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.View
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import app.R
import app.ext.BaseView
import app.ext.MarginItemDecoration
import app.features.currency.dagger.CurrencyScope
import app.model.viewdata.currency.CurrencyViewData
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_currency.view.*
import javax.inject.Inject


@SuppressLint("ViewConstructor")
@CurrencyScope
class CurrencyView @Inject constructor(
    val fragment: Fragment,
    private val currencyAdapter: CurrencyAdapter
) : BaseView(fragment.requireContext()) {

    init {
        View.inflate(context, R.layout.fragment_currency, this)
        setAdapter()
    }

    fun currencyItemsClicks(): Observable<CurrencyViewData> {
        return currencyAdapter.currencySelectionObservable
    }

    private fun setAdapter() {
        val loadLayoutAnimation =
            AnimationUtils.loadLayoutAnimation(fragment.requireContext(), R.anim.layout_slide_from_right)

        recyclerViewCurrency.apply {
            addItemDecoration(
                MarginItemDecoration(
                    resources.getDimensionPixelSize(R.dimen.spacing_xx_small)
                )
            )
            adapter = currencyAdapter
            layoutAnimation = loadLayoutAnimation
        }
    }

    fun setCurrencies(
        selectedCurrency: CurrencyViewData,
        currencies: List<CurrencyViewData>,
        withAnimation: Boolean
    ) {
        val sortedList = currencies.sortedWith(compareBy(CurrencyViewData::title)).toMutableList()
        sortedList[0] = selectedCurrency
        currencyAdapter.addData(sortedList)
        if (withAnimation) {
            recyclerViewCurrency.scheduleLayoutAnimation()
        }
    }

    fun scrollToTop() {
        recyclerViewCurrency.scrollToPosition(0)
    }
}
