package app.features.currency.mvp

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import app.R
import app.extensions.openKeyboard
import app.model.viewdata.currency.CurrencyViewData
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.row_item_currency.view.*
import org.jetbrains.anko.textColor
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.concurrent.TimeUnit
import android.os.Handler


class CurrencyAdapter : RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>() {

    private val currencies = mutableListOf<CurrencyViewData>()
    private val currencyPublishSubject = PublishSubject.create<CurrencyViewData>()

    val currencySelectionObservable: Observable<CurrencyViewData>
        get() = currencyPublishSubject

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_item_currency, parent, false)
        return CurrencyViewHolder(itemView)
    }

    override fun getItemCount() = currencies.size

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(currencies[0], currencies[position], position == 0)
    }

    fun addData(items: List<CurrencyViewData>) {
        currencies.apply {
            clear()
            addAll(items)
        }
        notifyDataSetChanged()
    }

    inner class CurrencyViewHolder(
        private val itemViewLayout: View
    ) : RecyclerView.ViewHolder(itemViewLayout) {

        private fun setTitle(title: String) {
            itemViewLayout.title.text = title
        }

        private fun setValue(view: AppCompatEditText, value: BigDecimal) {
            view.apply {
                setText(value.toInt().toString())
                setSelection(this.value.text!!.length)
                Handler().post {
                    isFocusableInTouchMode = true
                    requestFocus()
                    openKeyboard()
                }
            }
        }

        @SuppressLint("SetTextI18n")
        private fun setValue(baseValue: BigDecimal, value: BigDecimal) {
            val calculated = (baseValue * value).setScale(2, RoundingMode.HALF_EVEN)
            itemViewLayout.value.setText(calculated.toString())
        }

        private fun setCurrencyValue(
            baseCurrency: String,
            currencyValue: BigDecimal
        ) {
            val roundedValue = currencyValue.setScale(2, RoundingMode.HALF_EVEN)
            itemViewLayout.currencyValue.text = itemViewLayout.context.getString(R.string.currency_value_placeholder, baseCurrency, roundedValue.toString())
        }

        private fun moveTop() {
            layoutPosition.takeIf { it < currencies.size - 1 }?.also { currentPosition ->
                currencies.removeAt(currentPosition).also {
                    currencies.add(0, it.copy(value = 10.toBigDecimal()))
                }
                notifyItemMoved(currentPosition, 0)
                notifyItemChanged(0)
                notifyItemChanged(1)
            }
        }

        fun bind(base: CurrencyViewData, data: CurrencyViewData, selected: Boolean) {
            itemViewLayout.apply {
                if (selected) {
                    background = ContextCompat.getDrawable(context, R.drawable.bg_item_selected_selector)
                    title.textColor = ContextCompat.getColor(context, android.R.color.white)
                    value.textColor = ContextCompat.getColor(context, android.R.color.white)
                    currencyValue.isVisible = false
                    let {
                        setTitle(data.title)
                        setValue(this.value, data.value)

                        clicks().subscribe()
                        this.value.isEnabled = true
                        this.value
                            .textChanges()
                            .debounce(TEXT_CHANGES_DELAY, TimeUnit.MILLISECONDS)
                            .map(CharSequence::toString)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { value ->
                                if (adapterPosition == 0) {
                                    if (value != data.value.toString()) {
                                        val count = if (!value.isNullOrBlank()) {
                                            value.toBigDecimal()
                                        } else {
                                            0.toBigDecimal()
                                        }
                                        updateCurrencyValues(count)
                                    }
                                }
                            }
                    }
                } else {
                    background = ContextCompat.getDrawable(context, R.drawable.bg_item_normal_selector)
                    title.textColor = ContextCompat.getColor(context, android.R.color.black)
                    value.textColor = ContextCompat.getColor(context, android.R.color.black)
                    currencyValue.isVisible = true
                    let {
                        setTitle(data.title)
                        setValue(base.value, data.value)
                        setCurrencyValue(base.title, data.value)

                        this.value.isEnabled = false
                        this.value.textChanges()
                        clicks()
                            .map { data }
                            .doAfterNext { moveTop() }
                            .subscribe(currencyPublishSubject)
                    }
                }
            }
        }
    }

    private fun updateCurrencyValues(count: BigDecimal) {
        currencies[0] = currencies[0].copy(value = count)
        notifyItemChanged(0)
        currencies.forEachIndexed { index, _ ->
            if (index > 0) {
                notifyItemChanged(index)
            }
        }
    }

    companion object {
        private const val TEXT_CHANGES_DELAY = 500L
    }
}
