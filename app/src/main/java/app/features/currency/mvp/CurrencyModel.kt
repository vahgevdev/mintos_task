package app.features.currency.mvp

import app.features.currency.dagger.CurrencyScope
import app.model.viewdata.currency.CurrencyViewData
import domain.interactors.currency.GetLatestCurrenciesUseCase
import io.reactivex.Single
import javax.inject.Inject

@CurrencyScope
class CurrencyModel @Inject internal constructor(
    private val getLatestCurrenciesUseCase: GetLatestCurrenciesUseCase
) {
    var selectedCurrency = CurrencyViewData(BASE_CURRENCY, BASE_CURRENCY_VALUE)
    var latestCurrencies = mutableListOf<CurrencyViewData>()

    fun getLatestCurrencies(
        selectedCurrency: String
    ): Single<List<CurrencyViewData>> {
        return getLatestCurrenciesUseCase[selectedCurrency]
            .map { CurrencyViewData.toViewData(it) }
    }

    companion object {
        private const val BASE_CURRENCY = "EUR"
        val BASE_CURRENCY_VALUE = 10.toBigDecimal()
        const val REPEAT_TIME = 3L
        const val DELAY_DURATION = 300L
    }
}
