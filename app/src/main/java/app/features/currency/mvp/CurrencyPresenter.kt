package app.features.currency.mvp

import android.util.Log
import androidx.lifecycle.Lifecycle.Event.*
import androidx.lifecycle.OnLifecycleEvent
import app.ext.FragmentPresenter
import app.extensions.sameContentWith
import app.features.currency.dagger.CurrencyScope
import app.features.currency.mvp.CurrencyModel.Companion.BASE_CURRENCY_VALUE
import app.features.currency.mvp.CurrencyModel.Companion.DELAY_DURATION
import app.features.currency.mvp.CurrencyModel.Companion.REPEAT_TIME
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@CurrencyScope
class CurrencyPresenter @Inject internal constructor(
    private val view: CurrencyView,
    private val model: CurrencyModel
) : FragmentPresenter() {

    private val disposables by lazy { CompositeDisposable() }

    @OnLifecycleEvent(ON_CREATE)
    override fun onCreate() {
        disposables += observeLatestCurrencies(firstLoad = true, withDelay = false)
        disposables += observePeriodicSyncCurrencies()
    }

    @OnLifecycleEvent(ON_RESUME)
    override fun onResume() {
        setupListeners()
    }

    private fun setupListeners() {
        disposables += observeCurrencyItemsClicks()
    }

    private fun observePeriodicSyncCurrencies(): Disposable {
        return Flowable.interval(REPEAT_TIME, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                disposables += observeSyncLatestCurrencies()
            }
    }

    private fun observeSyncLatestCurrencies(): Disposable {
        model.selectedCurrency.let { selectedCurrency ->
            return model.getLatestCurrencies(selectedCurrency.title)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = { latestCurrencies ->
                        if (!(latestCurrencies sameContentWith model.latestCurrencies)) {
                            model.latestCurrencies.apply {
                                clear()
                                addAll(latestCurrencies)
                            }
                            view.setCurrencies(selectedCurrency, latestCurrencies, false)
                        }
                    },
                    onError = { Log.e(TAG, "Error while observing latest currencies for ${selectedCurrency.title}.") }
                )
        }
    }

    private fun observeLatestCurrencies(
        firstLoad: Boolean,
        withDelay: Boolean
    ): Disposable {
        model.selectedCurrency.let { selectedCurrency ->
            return model.getLatestCurrencies(selectedCurrency.title)
                .delay(if (withDelay) DELAY_DURATION else 0, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = { latestCurrencies ->
                        model.latestCurrencies.apply {
                            clear()
                            addAll(latestCurrencies)
                        }
                        view.setCurrencies(selectedCurrency, latestCurrencies, firstLoad)
                    },
                    onError = { Log.e(TAG, "Error while observing latest currencies for ${selectedCurrency.title}.") }
                )
        }
    }

    private fun observeCurrencyItemsClicks(): Disposable {
        return view.currencyItemsClicks()
            .subscribeBy(
                onNext = {
                    view.scrollToTop()
                    model.selectedCurrency = it.copy(value = BASE_CURRENCY_VALUE)
                    disposables += observeLatestCurrencies(firstLoad = false, withDelay = true)
                },
                onError = { Log.e(TAG, "Error while observing currency items clicks.") }
            )
    }

    @OnLifecycleEvent(ON_DESTROY)
    override fun onDestroy() {
        disposables.clear()
    }

    companion object {
        private const val TAG = "CurrencyPresenter"
    }
}
