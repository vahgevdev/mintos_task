package app.features.currency.dagger

import app.features.currency.CurrencyFragment
import data.dagger.builder.SubcomponentBuilder
import dagger.Subcomponent
import data.dagger.injection.Injection

@CurrencyScope
@Subcomponent(modules = [CurrencyModule::class])
interface CurrencyComponent : Injection<CurrencyFragment> {

    @Subcomponent.Builder
    interface Builder : SubcomponentBuilder<CurrencyComponent> {
        fun currencyModule(currencyModule: CurrencyModule): Builder
    }
}
