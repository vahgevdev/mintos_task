package app.features.currency.dagger

import androidx.fragment.app.Fragment
import app.features.currency.mvp.CurrencyAdapter
import dagger.Module
import dagger.Provides

@Module
class CurrencyModule(
    @get:Provides
    @get:CurrencyScope
    val fragment: Fragment
) {
    @Provides
    @CurrencyScope
    fun provideCurrencyAdapter(): CurrencyAdapter {
        return CurrencyAdapter()
    }
}
