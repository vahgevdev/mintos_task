package app.features.currency

import app.ext.BaseFragment
import app.features.currency.dagger.CurrencyComponent
import app.features.currency.dagger.CurrencyModule
import app.features.currency.dagger.CurrencyScope
import app.features.currency.mvp.CurrencyPresenter
import app.features.currency.mvp.CurrencyView
import app.features.main.MainActivity
import javax.inject.Inject

@CurrencyScope
class CurrencyFragment : BaseFragment<CurrencyPresenter>() {

    @Inject
    internal lateinit var view: CurrencyView

    @Inject
    internal lateinit var presenter: CurrencyPresenter

    private lateinit var currencyComponent: CurrencyComponent

    override fun getPresenter() = presenter

    override fun getView() = view

    override fun injectDependencies() {
        activity?.let {
            currencyComponent = (it as MainActivity).getComponent()
                .currencyComponentBuilder()
                .currencyModule(CurrencyModule(this))
                .build()
            currencyComponent.inject(this)
        }
    }
}
