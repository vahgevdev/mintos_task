package app.features.main.mvp

import app.ext.Presenter
import app.features.main.dagger.MainScope
import javax.inject.Inject

@MainScope
class MainPresenter @Inject internal constructor(
    private val view: MainView,
    private val model: MainModel
) : Presenter()
