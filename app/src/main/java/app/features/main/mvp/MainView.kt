package app.features.main.mvp

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import app.R
import app.ext.BaseView
import app.features.main.dagger.MainScope
import javax.inject.Inject

@SuppressLint("ViewConstructor")
@MainScope
class MainView @Inject constructor(
    activity: AppCompatActivity
) : BaseView(activity) {

    init {
        inflate(context, R.layout.activity_main, this)
        activity.title = activity.getString(R.string.title_currency_converter)
    }
}
