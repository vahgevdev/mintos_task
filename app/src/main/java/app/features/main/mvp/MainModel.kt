package app.features.main.mvp

import androidx.appcompat.app.AppCompatActivity
import app.features.main.dagger.MainScope
import javax.inject.Inject

@MainScope
class MainModel @Inject internal constructor(
    private val activity: AppCompatActivity
)
