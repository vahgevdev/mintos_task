package app.features.main.dagger

import app.dagger.ApplicationComponent
import app.features.currency.dagger.CurrencyComponent
import app.features.main.MainActivity
import dagger.Component
import data.dagger.injection.Injection

@MainScope
@Component(modules = [MainModule::class], dependencies = [ApplicationComponent::class])
interface MainComponent : Injection<MainActivity> {
    fun currencyComponentBuilder(): CurrencyComponent.Builder
}
