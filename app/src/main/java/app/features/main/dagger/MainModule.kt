package app.features.main.dagger

import androidx.appcompat.app.AppCompatActivity
import app.features.currency.dagger.CurrencyComponent
import dagger.Module
import dagger.Provides

@Module(
    subcomponents = [
        CurrencyComponent::class
    ]
)
class MainModule(
    @get:Provides
    @get:MainScope
    val activity: AppCompatActivity
)
