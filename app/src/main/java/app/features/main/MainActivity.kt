package app.features.main

import app.AndroidApp
import app.ext.BaseActivity
import app.ext.Presenter
import app.features.main.dagger.DaggerMainComponent
import app.features.main.dagger.MainComponent
import app.features.main.dagger.MainModule
import app.features.main.dagger.MainScope
import app.features.main.mvp.MainPresenter
import app.features.main.mvp.MainView
import javax.inject.Inject

@MainScope
class MainActivity : BaseActivity<Presenter>() {

    @Inject
    internal lateinit var view: MainView

    @Inject
    internal lateinit var presenter: MainPresenter

    private var mainComponent: MainComponent? = null

    override fun getPresenter() = presenter

    override fun getView() = view

    internal fun getComponent(): MainComponent {
        mainComponent?.let {
            return it
        }

        val component = DaggerMainComponent.builder()
            .applicationComponent(AndroidApp[this].component)
            .mainModule(MainModule(this))
            .build()

        mainComponent = component
        return component
    }

    override fun injectDependencies() {
        getComponent().inject(this)
    }
}
