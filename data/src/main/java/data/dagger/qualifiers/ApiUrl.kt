package data.dagger.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class ApiUrl
