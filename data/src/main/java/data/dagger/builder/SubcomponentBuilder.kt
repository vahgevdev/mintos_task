package data.dagger.builder

interface SubcomponentBuilder<out T> {
    fun build(): T
}
