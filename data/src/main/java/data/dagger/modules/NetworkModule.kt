package data.dagger.modules

import data.dagger.qualifiers.ApiUrl
import data.dagger.scopes.AppScope
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import data.dagger.qualifiers.RetrofitQualifier
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    @AppScope
    @Provides
    @RetrofitQualifier
    internal fun provideRetrofit(@ApiUrl apiUrl: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(apiUrl)
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }
}
