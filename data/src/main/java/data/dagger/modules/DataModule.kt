package data.dagger.modules

import dagger.Module
import dagger.Provides
import data.dagger.qualifiers.RetrofitQualifier
import data.dagger.scopes.AppScope
import data.net.RestApi
import data.net.RestApiBuilder
import data.repository.CurrencyDataRepository
import domain.repository.CurrencyRepository
import retrofit2.Retrofit

@Module
class DataModule {

    @AppScope
    @Provides
    internal fun provideCurrencyEndpoints(@RetrofitQualifier retrofit: Retrofit): RestApi.Currency {
        return RestApiBuilder(retrofit).build(RestApi.Currency::class.java)
    }

    @AppScope
    @Provides
    internal fun provideCurrencyRepository(currencyDataRepository: CurrencyDataRepository): CurrencyRepository {
        return currencyDataRepository
    }
}
