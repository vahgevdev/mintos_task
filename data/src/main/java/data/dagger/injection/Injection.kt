package data.dagger.injection

interface Injection<in T> {
    fun inject(t: T)
}
