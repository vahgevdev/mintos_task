package data.net

import retrofit2.Retrofit
import javax.inject.Inject

class RestApiBuilder @Inject constructor(
    private val retrofit: Retrofit
) {

    fun <T> build(clazz: Class<T>): T {
        validateServiceInterface(clazz)
        return retrofit.create(clazz)
    }

    private fun <T> validateServiceInterface(service: Class<T>) {
        if (!service.isInterface) {
            throw IllegalArgumentException("API declarations must be interfaces.")
        }

        if (service.interfaces.isNotEmpty()) {
            throw IllegalArgumentException("API interfaces must not extend other interfaces.")
        }
    }
}
