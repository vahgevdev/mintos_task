package data.net

import data.dto.CurrencyApiDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

/**
 * RestApi for retrieving data from the network.
 */
class RestApi {

    interface Currency {

        @Headers("Accept: application/json", "Connection: close")
        @GET("/latest")
        fun latestCurrencies(@Query("base") base: String) : Single<CurrencyApiDTO>
    }
}
