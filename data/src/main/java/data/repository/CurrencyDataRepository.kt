package data.repository

import data.dagger.scopes.AppScope
import data.dto.CurrencyApiDTO
import data.net.RestApi
import domain.model.currency.CurrencyModel
import domain.repository.CurrencyRepository
import io.reactivex.Single
import javax.inject.Inject

@AppScope
class CurrencyDataRepository @Inject internal constructor(
    private val api: RestApi.Currency
) : CurrencyRepository {

    override fun loadCurrencies(base: String): Single<CurrencyModel> {
        return api.latestCurrencies(base)
            .map(CurrencyApiDTO.Companion::toModel)
    }
}
