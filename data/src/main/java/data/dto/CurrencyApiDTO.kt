package data.dto

import com.google.gson.annotations.SerializedName
import domain.model.currency.CurrencyModel
import java.math.BigDecimal

data class CurrencyApiDTO(
    @SerializedName(value = "rates")
    val rates: Map<String, BigDecimal>,
    @SerializedName(value = "base")
    val base: String,
    @SerializedName(value = "date")
    val date: String
) {
    companion object {

        fun toModel(from: CurrencyApiDTO): CurrencyModel {
            return CurrencyModel(
                rates = from.rates,
                base = from.base,
                date = from.date
            )
        }
    }
}
