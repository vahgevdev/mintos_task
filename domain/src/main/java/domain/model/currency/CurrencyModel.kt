package domain.model.currency

import java.math.BigDecimal

data class CurrencyModel(
    val rates: Map<String, BigDecimal>,
    val base: String,
    val date: String
)
