package domain.interactors

import domain.transformer.SchedulerTransformer
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

abstract class BaseUseCase {
    private lateinit var schedulerTransformer: SchedulerTransformer

    protected val isDebugLogsEnabled: Boolean
        get() = true

    @Inject
    fun setSchedulerTransformer(schedulerTransformer: SchedulerTransformer) {
        this.schedulerTransformer = schedulerTransformer
    }

    protected fun schedulerTransformer(): SchedulerTransformer {
        return schedulerTransformer
    }

    fun Completable.applySubscribeScheduler(): Completable {
        return schedulerTransformer.applySubscribeScheduler(this)
    }

    fun <T> Observable<T>.applySubscribeScheduler(): Observable<T> {
        return schedulerTransformer.applySubscribeScheduler(this)
    }

    fun <T> Single<T>.applySubscribeScheduler(): Single<T> {
        return schedulerTransformer.applySubscribeScheduler(this)
    }
}
