package domain.interactors

import io.reactivex.Single

/**
 * The Single class implements the Reactive Pattern for a single value response.
 * Single behaves the same as Observable except that it can only emit either a single successful value, or an error
 *
 * @param <T> The return value type
 * @param <P> The parameter being passed in
</P></T> */
abstract class SingleParameterisedUseCase<T, P> : BaseUseCase() {

    protected abstract fun build(params: P): Single<T>

    private fun make(params: P, wrap: Boolean): Single<T> {
        val single = wrapDebug(build(params))
        return if (wrap) wrap(single) else single
    }

    operator fun get(params: P): Single<T> {
        return make(params, true)
    }

    fun chain(params: P): Single<T> {
        return make(params, false)
    }

    private fun wrap(observable: Single<T>): Single<T> {
        return observable.compose(schedulerTransformer().applySingleSchedulers())
    }

    private fun wrapDebug(observable: Single<T>): Single<T> {
        return observable
    }

}
