package domain.interactors.currency

import domain.interactors.SingleParameterisedUseCase
import domain.model.currency.CurrencyModel
import domain.repository.CurrencyRepository
import io.reactivex.Single
import javax.inject.Inject

class GetLatestCurrenciesUseCase @Inject constructor(
    private val currencyRepository: CurrencyRepository
) : SingleParameterisedUseCase<CurrencyModel, String>() {

    override fun build(params: String): Single<CurrencyModel> {
        return currencyRepository.loadCurrencies(params)
    }
}
