package domain.repository

import domain.model.currency.CurrencyModel
import io.reactivex.Single

interface CurrencyRepository {

    fun loadCurrencies(base: String): Single<CurrencyModel>
}