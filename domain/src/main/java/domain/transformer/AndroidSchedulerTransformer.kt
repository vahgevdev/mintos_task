package domain.transformer

import domain.executor.PostExecutionThread
import domain.executor.ThreadExecutor
import io.reactivex.*
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class AndroidSchedulerTransformer @Inject constructor(
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : SchedulerTransformer {

    private val subscribeScheduler: Scheduler = Schedulers.from(threadExecutor)
    private val observeScheduler: Scheduler = postExecutionThread.scheduler

    override fun <T> applyObservableSchedulers(): ObservableTransformer<T, T> =
        ObservableTransformer { observable ->
            observable.subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)
        }

    override fun applyCompletableSchedulers(): CompletableTransformer =
        CompletableTransformer { completable ->
            completable.subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)
        }

    override fun <T> applySingleSchedulers(): SingleTransformer<T, T> =
        SingleTransformer { single ->
            single.subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)
        }

    override fun <T> applyFlowableSchedulers(): FlowableTransformer<T, T> =
        FlowableTransformer { flowable ->
            flowable.subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)
        }

    override fun applySubscribeScheduler(completable: Completable): Completable {
        return completable.subscribeOn(subscribeScheduler)
    }

    override fun <T> applySubscribeScheduler(single: Single<T>): Single<T> {
        return single.subscribeOn(subscribeScheduler)
    }

    override fun <T> applySubscribeScheduler(observable: Observable<T>): Observable<T> {
        return observable.subscribeOn(subscribeScheduler)
    }

    override fun <T> applySubscribeScheduler(flowable: Flowable<T>): Flowable<T> {
        return flowable.subscribeOn(subscribeScheduler)
    }
}
