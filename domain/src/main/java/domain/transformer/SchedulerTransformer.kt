package domain.transformer

import io.reactivex.*

interface SchedulerTransformer {

    fun <T> applyObservableSchedulers(): ObservableTransformer<T, T>

    fun applyCompletableSchedulers(): CompletableTransformer

    fun <T> applySingleSchedulers(): SingleTransformer<T, T>

    fun <T> applyFlowableSchedulers(): FlowableTransformer<T, T>

    fun applySubscribeScheduler(completable: Completable): Completable

    fun <T> applySubscribeScheduler(single: Single<T>): Single<T>

    fun <T> applySubscribeScheduler(observable: Observable<T>): Observable<T>

    fun <T> applySubscribeScheduler(flowable: Flowable<T>): Flowable<T>

}